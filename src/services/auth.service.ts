import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import AuthProvider = firebase.auth.AuthProvider;

@Injectable()
export class AuthService {
	private authState: any = null;

	
	constructor(private afAuth: AngularFireAuth) {
		afAuth.authState.subscribe(user => {
			this.authState = user;
		});
		afAuth.auth.setPersistence('session');
	}

	signInWithEmail(credentials) {
		console.log('Sign in with email');
		return this.afAuth.auth.signInWithEmailAndPassword(credentials.email,
			 credentials.password);
	}
	addUser(credentials) {
		console.log('resiter ok');
		return this.afAuth.auth.createUserWithEmailAndPassword(credentials.email, credentials.password);
	}

	signOut(){
		return this.afAuth.auth.signOut().then(()=>{
			alert('로그아웃되었습니다');
		}).catch((err)=>{
			alert(err)
		})
	}
	//비번변경
	changePassword(newPassword){
		let user = firebase.auth().currentUser;
		return user.updatePassword(newPassword).then(function() {
			alert('변경됬습니다.');
		}).catch((err) => {
			alert(err)
		});
	}
	//삭제
	deleteAuth(){
		let user = firebase.auth().currentUser;
		return user.delete().then(function() {
			alert('삭제됬습니다.');
		}).catch((err) => {
			alert(err)
		});
	}
// 먼가 재인증해야 한다고했지만 그냥 된거같음 필요없지만 그냥 남겨듬 왜냐 그냥 남겨두고 싶었음
	reAuth(){
		let user = firebase.auth().currentUser;
		let credential;
		
		user.reauthenticateAndRetrieveDataWithCredential(credential).then(function() {

		}).catch(function(error) {

		});
	}

	get authenticated(): boolean {
    return this.authState !== null;
  }

  // 접속여부
  get currentUser(): any {
    return this.authenticated ? this.authState : null;
  }

  // 그냥 리턴
  get currentUserObservable(): any {
    return this.authState
  }

  // uid리턴
  get currentUserId(): string {
    return this.authenticated ? this.authState.uid : '';
  }

  // is anonymous?
  get currentUserAnonymous(): boolean {
    return this.authenticated ? this.authState.isAnonymous : false
	}

}