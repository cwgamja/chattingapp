import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-secret-worry-read',
  templateUrl: './secret-worry-read.page.html',
  styleUrls: ['./secret-worry-read.page.scss'],
})
export class SecretWorryReadPage implements OnInit {

  private pushkey: string;
  public apost =[];
  constructor(
    private afDB: AngularFireDatabase,
    public activeroute: ActivatedRoute,
    private router: Router
  ) {
    this.activeroute.params.subscribe((data:any)=>{
      this.pushkey = data.readKey;
    })
    this.getRoom(this.pushkey)
    .then(res=>{console.log(res)})
    .catch(err=>{console.log(err)})
  }
  

  ngOnInit() {
    
  }
  
  getRoom(pushkey){
    let tmp = this.afDB.database.ref('secret/').child(pushkey)
    return tmp.once('value')
    .then(snapshot=>{
      let result = snapshot.val();
      console.log(result)
      this.apost.push({
        title: result.title,
        script: result.script
      })
      return console.log('done')
    })
    .catch(err=>{console.log(err)})
  }
  goBack(){
    this.router.navigateByUrl('/tabs/tab2')
  }


}
