import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretWorryReadPage } from './secret-worry-read.page';

describe('SecretWorryReadPage', () => {
  let component: SecretWorryReadPage;
  let fixture: ComponentFixture<SecretWorryReadPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretWorryReadPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretWorryReadPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
