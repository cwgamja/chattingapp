import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { SecretWorryReadPage } from './secret-worry-read.page';

const routes: Routes = [
  {
    path: '',
    component: SecretWorryReadPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [SecretWorryReadPage]
})
export class SecretWorryReadPageModule {}
