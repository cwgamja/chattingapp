import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';

const routes: Routes = [
  //{ path: '', loadChildren: './tabs/tabs.module#TabsPageModule' },
  //{ path: 'login', loadChildren: './login/login.module#LoginPageModule' }
  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
  { path: '', loadChildren: './tabs/tabs.module#TabsPageModule', canActivate: [AuthGuard] },
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'point-shop', loadChildren: './point-shop/point-shop.module#PointShopPageModule' },
  { path: 'profile-setting', loadChildren: './profile-setting/profile-setting.module#ProfileSettingPageModule' },
  { path: 'point-shop', loadChildren: './point-shop/point-shop.module#PointShopPageModule' },
  { path: 'tospage', loadChildren: './tospage/tospage.module#TOSPagePageModule' },
  { path: 'receive-settings', loadChildren: './receive-settings/receive-settings.module#ReceiveSettingsPageModule' },
  { path: 'worry-write', loadChildren: './worry-write/worry-write.module#WorryWritePageModule', },
  { path: 'worry-read', loadChildren: './worry-read/worry-read.module#WorryReadPageModule' },  { path: 'secret-worry-write', loadChildren: './secret-worry-write/secret-worry-write.module#SecretWorryWritePageModule' },
  { path: 'secret-worry-read', loadChildren: './secret-worry-read/secret-worry-read.module#SecretWorryReadPageModule' },



];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
