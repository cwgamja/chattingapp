import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { FCM } from '@ionic-native/fcm/ngx';
import { AngularFireDatabase } from 'angularfire2/database';


import { AdmobFreeService } from '../service/addmobfree.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  loginForm: FormGroup;
  loginError: string;
  constructor(private fcm:FCM,
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private afDB: AngularFireDatabase,
    private router: Router,
    private admobFreeService: AdmobFreeService
    ) { }

  ngOnInit() {
    this.admobFreeService.BannerAd();
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
			password: ['', Validators.compose([Validators.required, Validators.minLength(6)])]
    })
  }

  onSubmit(){
    console.log('login')
    let data = this.loginForm.value;

		if (!data.email) {
      alert('이메일입력을 하세여;')
			return;
		}
		let credentials = {
			email: data.email,
			password: data.password
		};
		this.auth.signInWithEmail(credentials)
			.then(
        () =>{
          let uid = this.auth.currentUserId
          let newfcm = this.afDB.database.ref('/users/').child(uid);
          this.fcm.getToken()
          .then(token=>{
            newfcm.update(
              {
                fcmkey: token
              }
            )
          }).catch((err)=>{
            newfcm.update(
              {
                fcmkey:'notdevice'
              }
            )
          })
          this.router.navigateByUrl('/tabs')
        } ,
        error => {
          this.loginError = error.message
          alert(this.loginError);
        }
			);
    
  }
  goToRegiter(){
    this.router.navigateByUrl('/register')
  }
}
