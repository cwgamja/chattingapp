import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tospage',
  templateUrl: './tospage.page.html',
  styleUrls: ['./tospage.page.scss'],
})
export class TOSPagePage implements OnInit {

  constructor(private router: Router) { }

  ngOnInit( ) {
  }
  goToBack(){ 
    this.router.navigateByUrl('/tabs/tab4');
  }

}
