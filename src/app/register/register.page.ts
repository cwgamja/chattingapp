import { FormBuilder, FormGroup, Validators, EmailValidator } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList} from "angularfire2/database"; 
import { auth } from 'firebase';
import { FCM } from '@ionic-native/fcm/ngx';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  registerForm: FormGroup;
  registerError: string;
  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private afDB: AngularFireDatabase,
    private fcm: FCM
 ) {}

  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required ,Validators.maxLength(6)])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
      age:  ['',Validators.compose([Validators.pattern('[0-9]*'),Validators.required])],
      gender: ['',[Validators.required]]
    });
  }

  async signUp(){
    //registerForm 제대로 입력안하면 출력
    if (this.registerForm.invalid) {
      alert('제대로 값을 입력해주세요.')
      return;
    }
 
    console.log('register')
    let data = this.registerForm.value;

		let credentials = {
      name: data.name,
			email: data.email,
      password: data.password,
    };

    this.auth.addUser(credentials).then(
      (credentials: any) => {
        console.log(credentials);
        console.log(credentials);
        
        let ref = this.afDB.database.ref('/users/count/');
        ref.once('value')
        .then(snapshot=>{
          let snap = snapshot.val();
          let val = snap.number;
          let tmpnum = val + 1;
          this.fcm.getToken()
          .then(token=>{
            this.afDB.list('/users/').set(credentials.user.uid,
              {
                uid: credentials.user.uid,
                name: data.name,
                email: data.email,
                age: data.age,
                gender: data.gender,
                randomkey: tmpnum,
                fcmkey:token
              }
            );
          })
          .catch(err=>{
            this.afDB.list('/users/').set(credentials.user.uid,
              {
                uid: credentials.user.uid,
                name: data.name,
                email: data.email,
                age: data.age,
                gender: data.gender,
                randomkey: tmpnum,
                fcmkey:'notdevide'
              }
            );
            alert('it is not android or ios')
          })
          
          this.afDB.database.ref('/users/count/').set({
            number: tmpnum
          })
        })
        alert('회원가입이 완료되었습니다.')
        this.router.navigateByUrl('/login')
        }
   ).catch(err => { // 가입시 에러 팝업
    console.log(err)
      if(err.code == 'auth/invalid-email' || err.code == 'auth/weak-password'){
        alert('제대로 입력해주세요.')
      }else if(err.code == 'auth/email-already-in-use'){
        alert('중복된 이메일입니다.')
      }else{
        alert('예기치 못한 오류가 발생했습니다.')
      }
   }
   );
    
  } 

  goToBack(){
    this.router.navigateByUrl('/login')
  }
}
