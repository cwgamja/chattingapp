import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReceiveSettingsPage } from './receive-settings.page';

const routes: Routes = [
  {
    path: '',
    component: ReceiveSettingsPage,
    children: [
      { path:'tab1',redirectTo: '/tabs/tab1'},
      { path:'tab2',redirectTo: '/tabs/tab2'},
      { path:'tab3',redirectTo: '/tabs/tab3'},
      { path:'tab4',redirectTo: '/tabs/tab4'},
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReceiveSettingsPage]
})
export class ReceiveSettingsPageModule {}
