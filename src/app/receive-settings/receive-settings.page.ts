import { AuthService } from '../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';
import { AlertController } from '@ionic/angular';
import { AngularFireDatabase} from "angularfire2/database"; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-receive-settings',
  templateUrl: './receive-settings.page.html',
  styleUrls: ['./receive-settings.page.scss'],
})
export class ReceiveSettingsPage implements OnInit {
  susin : any;
  susinAge : any;
  susinGender : any;
  susinAll: any;
  
  allReceive: boolean;
  notReceive: boolean;
  selectReceive: boolean;
  allGender : boolean;
  man : boolean;
  woman : boolean;
  all : boolean;
  teens : boolean;
  twenties: boolean;
  useSelecte : boolean;
  

  constructor(
    private auth: AuthService,
    public alertController: AlertController,
    private afDB: AngularFireDatabase,
    private router: Router
  ) {
  }



  //고민수신 
  SelectAll(susinNumber){
    this.useSelecte = true;
    this.all=false;
    this.allGender=false;
    this.teens=false;
    this.twenties=false;
    this.man=false;
    this.woman=false;
    this.selectReceive=false;
    this.notReceive=false;

    this.susinAll = susinNumber;
    this.susinGender = 0;
    this.susinAge = 0;
  }
  //비수신
  SelectNot(susinNumber){
    this.useSelecte = true;
    this.all=false;
    this.allGender=false;
    this.teens=false;
    this.twenties=false;
    this.man=false;
    this.woman=false;
    this.allReceive=false;
    this.selectReceive=false;

    this.susinAll = susinNumber;
    this.susinGender = 0;
    this.susinAge = 0;
  }
  //선택수신
  SelecteAble(){
    this.notReceive=false;
    this.useSelecte = false;
    this.all=true;
    this.allGender=true;
    this.teens=false;
    this.twenties=false;
    this.man=false;
    this.woman=false;
    this.allReceive=false;
    

    this.susinGender = 10;
    this.susinAge = 1;
    this.susinAll = 0;
  }

  genderAll(susinNumber){
    this.man=false;
    this.woman=false;
    this.susinGender = susinNumber;
  }
  genderMan(susinNumber){
    this.allGender=false;
    this.woman=false;
    this.susinGender = susinNumber;
  }
  genderWoman(susinNumber){
    this.allGender=false;
    this.man=false;
    this.susinGender = susinNumber;
  }
  allAge(susinNumber){
    this.teens=false;
    this.twenties=false;
    this.susinAge = susinNumber;
  }
  teensAge(susinNumber){
    this.all=false;
    this.twenties=false;
    this.susinAge = susinNumber;
  }
  twentiesAge(susinNumber){
    this.all=false;
    this.teens=false;
    this.susinAge = susinNumber;
  }
  
// 간단하게 바꿈 수신 저장부분 전체:0 비수신:1  선택은 11~33
  saveSusin(){
    this.susin = this.susinAge + this.susinGender + this.susinAll;
    firebase.database().ref('users/' + this.auth.currentUserId).on('value', (snapshot) => {
      let result = snapshot.val();
      this.afDB.list('/users/' ).set(this.auth.currentUserId,
        {
          age : result.age,
          email : result.email,
          gender : result.gender,
          name: result.name,            
          susin : this.susin,
          uid : result.uid,
          randomkey : result.randomkey,
        }
      );
    }); 
    console.log(this.susin)
    alert('저장했습니다.');
  }
    ngOnInit() {
      //기본값
      this.SelectAll(0)
      this.allReceive=true;
    }
    goToBack(){
      this.router.navigateByUrl('/tabs/tab4');
    }

  }


  