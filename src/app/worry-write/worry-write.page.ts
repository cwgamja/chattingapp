import { Component, OnInit} from '@angular/core';
import { AngularFireDatabase} from "angularfire2/database"; 
import { AngularFireAuth } from 'angularfire2/auth';
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AlertController} from '@ionic/angular';
import { AuthService } from '../../services/auth.service'

import { AdmobFreeService } from '../service/addmobfree.service';

@Component({
  selector: 'app-worry-write',
  templateUrl: './worry-write.page.html',
  styleUrls: ['./worry-write.page.scss'],
})
export class WorryWritePage implements OnInit {
  openWorryTitle: string;
  openWorryScript: string;
  cate: string; 
  userName: any;
  private user: firebase.User;

  constructor(
    private alertCtrl: AlertController,
    private afDB: AngularFireDatabase,
    private router: Router,
    public afAuth: AngularFireAuth,
    private admobFreeService: AdmobFreeService,
    public authService: AuthService,
    ) { 
        afAuth.authState.subscribe(user => {
        this.user = user;
    });
  }

  ngOnInit() {
    this.showInterstitial();
    this.getData()    
  }
  showInterstitial(){
    this.admobFreeService.InterstitialAd();
  }
// 이름 가져오기
  getData(){
    firebase.database().ref('users/'+ this.authService.currentUserId).on('value', (snapshot) => {
      let result = snapshot.val().name
      this.userName = result
        console.log(this.userName)
    });
  }
  // 작성시 팝업띄우고 확인하렴
  showConfirmAlert() {
    let alert = this.alertCtrl.create({
        message: '작성 하시겠습니까?',
        buttons: [
            {
                text: '네',
                handler: () => {
                    this.worryWrite()
                }
            },
            {
                text: '아니오',
                handler: () => {
                }
            }]}).then(alert => {
                alert.present();
            });
  }

  worryWrite(){
    let serverTime = firebase.database.ServerValue.TIMESTAMP;


// 내용 openroom/카테고리에 저장하기
    let roomKey = this.afDB.list('/openroom/' + this.cate).push(
      {
        uid: this.authService.currentUserId,
        cate: this.cate,
        title: this.openWorryTitle,
        script: this.openWorryScript,
        time: serverTime,
        name: this.userName,
        reportvote: 0
      }
    );
//my룸에 보여줄 정보 저장하기
      this.afDB.list('/openmyroom/' + this.authService.currentUserId).set(roomKey.key,
      {
        uid: this.authService.currentUserId,
        cate: this.cate,
        title: this.openWorryTitle,
        script: this.openWorryScript,
        time: serverTime,
        name: this.userName,
        reportvote: 0
      }
    );

   this.router.navigateByUrl('/tabs');
  }
  goToBack(){
    this.router.navigateByUrl('/tabs');
  }
}