import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorryWritePage } from './worry-write.page';

describe('WorryWritePage', () => {
  let component: WorryWritePage;
  let fixture: ComponentFixture<WorryWritePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorryWritePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorryWritePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
