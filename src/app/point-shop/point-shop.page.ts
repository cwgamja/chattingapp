import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-point-shop',
  templateUrl: './point-shop.page.html',
  styleUrls: ['./point-shop.page.scss'],
})
export class PointShopPage implements OnInit {

  constructor(    private router: Router,
    ) { }

  ngOnInit() {
  }
  
  goToBack(){
    this.router.navigateByUrl('/tabs/tab4');
  }

}

