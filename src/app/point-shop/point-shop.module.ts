import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { PointShopPage } from './point-shop.page';

const routes: Routes = [
  {
    path: '',
    component: PointShopPage,
    children: [
      { path:'tab1',redirectTo: '/tabs/tab1'},
      { path:'tab2',redirectTo: '/tabs/tab2'},
      { path:'tab3',redirectTo: '/tabs/tab3'},
      { path:'tab4',redirectTo: '/tabs/tab4'},
    ]
  }
];

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [PointShopPage]
})
export class PointShopPageModule {}
