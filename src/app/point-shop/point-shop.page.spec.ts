import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PointShopPage } from './point-shop.page';

describe('PointShopPage', () => {
  let component: PointShopPage;
  let fixture: ComponentFixture<PointShopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PointShopPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PointShopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
