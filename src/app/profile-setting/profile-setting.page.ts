import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { AlertController} from '@ionic/angular';
import { AngularFireDatabase} from "angularfire2/database"; 
import * as firebase from 'firebase';

@Component({
  selector: 'app-profile-setting',
  templateUrl: './profile-setting.page.html',
  styleUrls: ['./profile-setting.page.scss'],
})
export class ProfileSettingPage implements OnInit {
  changePassword: string;
  changeNameValue: string;
  user = firebase.auth().currentUser;
  constructor(
    private router: Router,
    private auth:AuthService,
    private afDB: AngularFireDatabase,
    private alertCtrl: AlertController,) {
    
  }

  ngOnInit() {
  }
//비번 바꿉니다.
  changePasswordAlert(){

    let alert = this.alertCtrl.create({
      message: '변경 하시겠습니까?',
      buttons: [
          {
              text: '네',
              handler: () => {
                this.auth.changePassword(this.changePassword)
              }
          },
          {
              text: '아니오',
              handler: () => {
              }
          }]}).then(alert => {
              alert.present();
          });
  }
//이름변경
  changeNameAlert(){
    let alert = this.alertCtrl.create({
      message: '변경 하시겠습니까?',
      buttons: [
          {
              text: '네',
              handler: () => {
                this.changeName()
              }
          },
          {
              text: '아니오',
              handler: () => {
              }
          }]}).then(alert => {
              alert.present();
          });
  }
//이름 변경
  changeName(){
    firebase.database().ref('users/' + this.auth.currentUserId).on('value', (snapshot) => {
      let result = snapshot.val();
      this.afDB.list('/users/' ).set(this.auth.currentUserId,
        {
          age : result.age,
          email : result.email,
          gender : result.gender,
          name: this.changeNameValue,          
          susin : result.susin,
          uid : result.uid,
          randomkey : result.randomkey,
        }
      );
    });  
      alert('변경되었습니다.')
  }

  signOut(){
    this.auth.signOut();
    this.router.navigateByUrl('/login')
  }
//탈퇴
  deleteUser(){
    let alert = this.alertCtrl.create({
      message: '탈퇴 하시겠습니까?',
      buttons: [
          {
              text: '네',
              handler: () => {
                this.auth.deleteAuth()
              }
          },
          {
              text: '아니오',
              handler: () => {
              }
          }]}).then(alert => {
              alert.present();
          });
  }
  goToBack(){
    this.router.navigateByUrl('/tabs/tab4');
  }
}
