import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList} from "angularfire2/database"; 
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})


export class Tab2Page implements OnInit{
  items: AngularFireList<any>;
  public secretList =[];
  public mykeys =[];
  constructor(
    private afDB: AngularFireDatabase,
    private router: Router,
    private authservice: AuthService
    ) {
    this.items = afDB.list('users');
    
  }
  
  ngOnInit(){
    this.getpushkey(this.authservice.currentUserId)
    .then(value=>{
      for(let i in this.mykeys){
        this.getSecret(this.mykeys[i].pushkey)
      }
    })
    .catch(err=>{console.log(err)})
  }
  getSecret(pushkey){
    let secret = firebase.database().ref('secret/')
    secret.on('value', (snapshot)=>{
      let result = snapshot.val();
      for(let key in result){
        this.secretList.push({
          key: pushkey,
          title: result[key][pushkey].title,
          script: result[key][pushkey].script
        })
        
      }
    })
  }

  getpushkey(uid){
    let secret = firebase.database().ref('secretRoom/').orderByChild(uid)
    return secret.once('value')
    .then(snapshot=>{
      let tmp = snapshot.val()
      let res =[]
      for(let key in tmp){
        if(tmp[key][uid]){
          this.mykeys.push(tmp[key][uid])
          res.push(tmp[key][uid])
        }
      }
      
      return res;
    })
    .catch(err=>{console.log(err)})
    /*secret.on('value', (snapshot)=>{
      let result = snapshot.val();
      for(let i in result){
        this.secretList.push({
          key: Object.keys(snapshot.val())[i],
          title: result[i].title,
          script: result[i].script
        })
      }
      
    })*/
  }

  goToReadRoom(item){  
    this.router.navigate(['/secret-worry-read', { readKey: item.key }]);
//    this.router.navigateByUrl('/worry-read');
  }

  goWriteSecretWorry(){
    this.router.navigateByUrl('/secret-worry-write');
  }
}
