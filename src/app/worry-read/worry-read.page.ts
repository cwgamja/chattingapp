import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase} from "angularfire2/database"; 
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { Key, browser } from 'protractor';
import { AuthService } from '../../services/auth.service'
import { AlertController } from '@ionic/angular';
import { ConcatSource } from 'webpack-sources';

@Component({
  selector: 'app-worry-read',
  templateUrl: './worry-read.page.html',
  styleUrls: ['./worry-read.page.scss'],
})
export class WorryReadPage implements OnInit {
  private key: string ;
  private roomCate: string ;
  private user: any = null;
  private readData;
  private userName;
  private commentData;
  commentWrite: string;
  private result1;
  private result2;
  private report;

  
  constructor( 
    private afDB: AngularFireDatabase,
    private router: Router,
    public activateroute: ActivatedRoute,
    public afAuth: AngularFireAuth,
    public authService: AuthService,
    private alertCtrl: AlertController,) {
      // 코멘트에 사용할 네임 가져오기
      this.user = authService.currentUserObservable;

// 읽기에서 키,카테고리 가져오기 (보여주기위함)
      this.activateroute.params.subscribe((data: any) => {
        this.key = data.readKey;
        this.roomCate = data.cate;   
      });
      

    }

  ngOnInit() {
    this.getData()
  }
// 데이터 가져오기
  getData(){
    firebase.database().ref('users/'+ this.authService.currentUserId).on('value', (snapshot) => {
      let resultName = snapshot.val().name
      this.userName = resultName
      console.log(this.userName)
    });
    firebase.database().ref('openroom/' + this.roomCate +'/'+ this.key).on('value', (snapshot) => {
      let reportVote = snapshot.val().reportvote
      this.report = reportVote
      console.log(this.report)
    });
    
    let cateName = ['진로','연애','일상','기타']
//가져온 키, 카테고리로 제목,이름,내용 가져요기
    firebase.database().ref('openroom/' + this.roomCate +'/'+ this.key).on('value', (snapshot) => {
      let result = snapshot.val();
      this.readData = [];
        this.readData.push({
          name : result.name,
          title : result.title,
          cate: cateName[parseInt(this.roomCate)-1],
          script: result.script,
          reportvote: result.reportvote
        });
        console.log(snapshot.val())
    });

// 코멘트 가져와서 뿌려주기
    firebase.database().ref('openroom/' + this.roomCate + '/' + this.key + '/' + 'comment').on('value', (snapshot) => {
      let result = snapshot.val();
      var i=0;
      this.commentData = [];
     
      for(let k in result){
        this.commentData.push({
          name : result[k].name,
          comment : result[k].comment,
          goodComment: result[k].goodComment,
          badComment: result[k].badComment, 
          key : k
        });
        i++
      }
    });
  }
//댓글 작성팝업
  setCommentAlert(){
    let alert = this.alertCtrl.create({
      message: '작성 하시겠습니까?',
      buttons: [
          {
              text: '네',
              handler: () => {
                this.setComment()
              }
          },
          {
              text: '아니오',
              handler: () => {
              }
          }]}).then(alert => {
              alert.present();
          });
  }
  
// 고민 신고팝업
  reportAlert(){
    let alert = this.alertCtrl.create({
      message: '신고 하시겠습니까?',
      buttons: [
          {
              text: '네',
              handler: () => {
                this.reportWorry(this.key, this.report)
              }
          },
          {
              text: '아니오',
              handler: () => {
              }
          }]}).then(alert => {
              alert.present();
          });
  }
// 고민 신고 부분
  reportWorry(roomKey,reportvote){
    firebase.database().ref('/users/' + this.authService.currentUserId + '/' + 'reportworry').on('value', (snapshot) => {
      console.log(snapshot.val())
      this.result2 = snapshot.val();
    });
      console.log(this.result2[this.key])
      if(this.result2[this.key] != null){
        alert('이미 신고하셨습니다..');
        return;
      }
    
     this.afDB.list('/users/' + this.authService.currentUserId + '/' + 'reportworry').set(roomKey,
      {
        reCommendVote: 'true'
      }
    );
    if (reportvote < 10){
      this.afDB.list('/openroom/' + this.roomCate + '/' ).update(this.key,
        {
          reportvote: reportvote+1
        }
      );
    }else{ //블라인드처리 복구불가 그냥 신고10회면 아닥하고있어야댐
          this.afDB.list('/openroom/' + this.roomCate + '/' ).update(this.key,
            {
            reportvote: reportvote+1,
            script: '신고로 인해 블라인드된 글입니다.'
            }
          );
    }

  }


  setComment(){
    var serverTime = firebase.database.ServerValue.TIMESTAMP;
// 코멘트 작성하기
    this.afDB.list('/openroom/' + this.roomCate + '/' + this.key + '/' + 'comment').push(
      {
        uid: this.authService.currentUserId,
        comment: this.commentWrite,
        time: serverTime,
        name: this.userName,
        goodComment: 0,
        badComment: 0,
      });
  }
//투표값 증가 시키기
  goodComment(commentKey,goodValue){

    firebase.database().ref('/users/' + this.authService.currentUserId + '/' + 'recommend/' + this.key).on('value', (snapshot) => {
      let comentVote = snapshot.val();
      this.result1 = comentVote ;
      console.log(this.result1[commentKey].recommend)
    }); 

    let ref = firebase.database().ref('/users/' + this.authService.currentUserId + '/' + 'recommend/' + this.key);
    ref.once('value')
    .then(snapshot=>{
      let comentVote = snapshot.val();
      this.result1 = comentVote ;
      console.log(this.result1[commentKey])
    }).catch(err =>{
      this.afDB.list('/users/' + this.authService.currentUserId + '/' + 'recommend/'+ this.key).set(commentKey,
        {
          recommend: 'false'
        });
        this.goodComment(commentKey,goodValue);
    })

    if(this.result1[commentKey].recommend == 'true'){
      alert('이미 투표하셨습니다..');
      return;
    }


    this.afDB.list('/users/' + this.authService.currentUserId + '/' + 'recommend/'+ this.key).set(commentKey,
    {
      recommend: 'true'
    });
    this.afDB.list('/openroom/' + this.roomCate + '/' + this.key + '/comment/' ).update(commentKey,
      {
        goodComment: goodValue+1,
      }
    );

  }

  badComment(commentKey,badValue){

    firebase.database().ref('/users/' + this.authService.currentUserId + '/' + 'recommend/' + this.key).on('value', (snapshot) => {
      let comentVote = snapshot.val();
      this.result1 = comentVote ;
      console.log(this.result1[commentKey].recommend)
    }); 

    let ref = firebase.database().ref('/users/' + this.authService.currentUserId + '/' + 'recommend/' + this.key);
    ref.once('value')
    .then(snapshot=>{
      let comentVote = snapshot.val();
      this.result1 = comentVote ;
      console.log(this.result1[commentKey])
    }).catch(err =>{
      this.afDB.list('/users/' + this.authService.currentUserId + '/' + 'recommend/'+ this.key).set(commentKey,
        {
          recommend: 'false'
        });
        this.goodComment(commentKey,badValue);
    })

  
    if(this.result1[commentKey].recommend == 'true'){
      alert('이미 투표하셨습니다..');
      return;
    }

    this.afDB.list('/users/' + this.authService.currentUserId + '/' + 'recommend/'+ this.key).set(commentKey,
      {
        recommend: 'true'
      });
    if (badValue < 9){
      this.afDB.list('/openroom/' + this.roomCate + '/' + this.key + '/comment/' ).update(commentKey,
        {
        badComment: badValue + 1
        }
      );
    }else{ //블라인드처리 복구불가 그냥 신고10회면 아닥하고있어야댐
      for(let k in this.commentData){
        if(this.commentData[k].key == commentKey){
          this.afDB.list('/openroom/' + this.roomCate + '/' + this.key + '/comment/' ).update(commentKey,
            {
            badComment: badValue+1,
            comment: '블라인드된 댓글입니다.'
            }
          );
        }
      }
    }
  }

  goToBack(){
    this.router.navigateByUrl('/tabs');
  }

}
