import { Component, OnInit, ViewChild } from '@angular/core';
import { AngularFireDatabase, AngularFireList} from "angularfire2/database"; 
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { setFlagsFromString } from 'v8';
import { IonInfiniteScroll } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  selectedItem: any;
  icons: string[];
  items: AngularFireList<any>;
  userDataLsit = [];
  userData = [];
  latelyUserData = [];
  lastUserData = [];
  searchValue: string;
  addValue = [];
  listSort: string;
  ref: any;
  listCount:any;
  cate:any;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    private afDB: AngularFireDatabase,
    private router: Router) {
    this.items = afDB.list('users');
    this.listCount = 1;
  }

  ngOnInit(){
  //두번 클릭 방지 한번씩 실행해놓기
    this.getOpenWorry(4);
    this.getOpenWorry(3);
    this.getOpenWorry(2);
    this.getOpenWorry(1);
  }

//카테고리에 맞게 제목과 이름가져오기
  getOpenWorry(cate) {
    this.listSort="2"
    firebase.database().ref('openroom/' + cate).limitToLast(this.listCount*15).on('value', (snapshot) => { 
      if (this.cate == cate){
        this.cate = cate;
      }else{
        this.listCount = 1;
        this.cate = cate;
      }
  
      let result = snapshot.val();
      
      var i=0;
      this.userData = [];
      for(let k in result){
        this.userData.push({
          key : Object.keys(snapshot.val())[i],
          name : result[k].name,
          title : result[k].title,
          cate: cate
        });
        i++
      }

      this.userDataLsit = this.userData
      this.latelyUserData = this.userData.reverse()
      console.log(this.userData)
/*
      // 셀렉트 정렬 (지울부분)
      if (this.listSort=="2") {
        this.userDataLsit = this.latelyUserData
      }else if (this.listSort=="1") {
        this.userDataLsit = this.userDataLsit
      }
 */
    });
  }

  //위로 새로고침
  doRefresh(event) {
    console.log('새로고침');

    setTimeout(() => {
      console.log('완료');
      this.listCount=1;
      this.getOpenWorry(this.cate)
      event.target.complete();
    }, 1000);
  }
  //아래로 새로고침
  loadData(event) {
    console.log('아래');

    setTimeout(() => {
      console.log('아래완료');
      this.listCount++
      event.target.complete();
      this.getOpenWorry(this.cate)
      console.log(this.listCount);
    }, 1000);
  }

// 셀렉트 했을때 정렬 (지울부분)
/*
  onSelectChange(event){
    if (this.listSort=="2") {
      this.userDataLsit.reverse()
    }else if (this.listSort=="1") {
      this.userDataLsit.reverse()
    }
  } 
//검색하기 (지울부분)
  searchData(){
    this.addValue = [];
    for(let i in this.userData){
      if(this.userData[i].title.indexOf(this.searchValue) != -1){
        this.addValue.push({
          key : this.userData[i].key,
          name : this.userData[i].name,
          title : this.userData[i].title,
          cate: this.userData[i].cate
        })
      }
    }
  
    this.userDataLsit = this.addValue
  }
*/
  goToReadRoom(item){  
    console.log(item)
    this.router.navigate(['/worry-read', { readKey: item.key, cate: item.cate}]);
//    this.router.navigateByUrl('/worry-read');
  }
  goWriteWorry(){
    this.router.navigateByUrl('/worry-write');
  }
}
