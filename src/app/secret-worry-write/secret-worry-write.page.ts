import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireDatabase} from "angularfire2/database";
import { HttpClient } from '@angular/common/http'

import { AdmobFreeService } from '../service/addmobfree.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-secret-worry-write',
  templateUrl: './secret-worry-write.page.html',
  styleUrls: ['./secret-worry-write.page.scss'],
})
export class SecretWorryWritePage implements OnInit {
  Title: string;
  Script: string;
  private user:any= null;
  userName: string;

  constructor(private alertCtrl:AlertController, private afDB: AngularFireDatabase,private afAuth:AngularFireAuth, private router: Router, private authservice: AuthService, private http: HttpClient, private admobFreeService:AdmobFreeService) {
    afAuth.authState.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() {
    this.showInterstitial();
  }
  showInterstitial(){
    this.admobFreeService.InterstitialAd();
  }

  showConfirmAlert() {
    let alert = this.alertCtrl.create({
      message: '작성 하시겠습니까?',
      buttons: [
        {
          text: '네',
          handler: () => {
             this.worryWrite()
          }
        },
        {
         text: '아니오',
           handler: () => {
           }
        }]}).then(alert => {
          alert.present();
        });
  }

  goToBack(){
    this.router.navigateByUrl('/tabs/tab2');
  }

  worryWrite(){
    let serverTime = firebase.database.ServerValue.TIMESTAMP;
    let tosend = {
      uid : this.user.uid,
      title : this.Title,
      script : this. Script,
      time: serverTime
    }
    this.http.post('https://us-central1-cwchatting.cloudfunctions.net/entries/', tosend,{responseType: 'text'})
    .subscribe(res=>{
      alert('비밀 칭구 만들기 완료! >ㅁ<');
      this.router.navigateByUrl('/tabs/tab2')
    })
    /*let serverTime = firebase.database.ServerValue.TIMESTAMP;
    //작성자 이름 가져오기
    firebase.database().ref('users/' + this.authService.currentUserId).on('value', (snapshot) => {
      let result = snapshot.val();
      this.userName = result.name
    });

    let roomKey = this.afDB.list('/secretroom/' + this.authService.currentUserId).push(
      {
        uid: this.user.uid,
        title: this.Title,
        script: this.Script,
        time: serverTime,
        name: this.userName,
      }
    );

    this.afDB.list('/sentsecret/' + this.user.uid).set(roomKey.key,
      {
        uid: this.user.uid,
        title: this.Title,
        script: this.Script,
        time: serverTime,
        name: this.userName,
      }
    );*/

    

  }
}
