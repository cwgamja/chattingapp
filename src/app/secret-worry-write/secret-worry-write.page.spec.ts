import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecretWorryWritePage } from './secret-worry-write.page';

describe('SecretWorryWritePage', () => {
  let component: SecretWorryWritePage;
  let fixture: ComponentFixture<SecretWorryWritePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecretWorryWritePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecretWorryWritePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
