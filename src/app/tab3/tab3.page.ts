import { Component, OnInit } from '@angular/core';
import { AngularFireDatabase, AngularFireList} from "angularfire2/database"; 
import { Router } from '@angular/router';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';
import { AlertController} from '@ionic/angular';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {
  selectedItem: any;
  icons: string[];
  items: AngularFireList<any>;
  userData = [];
  ref: any;
  private user: firebase.User;

  constructor(
    private alertCtrl: AlertController,
    private afDB: AngularFireDatabase,
    private router: Router,
    public afAuth: AngularFireAuth,
    public authService: AuthService,
    
    ) {
      this.afAuth.authState.subscribe(user => {
        this.user = user;
      });
      this.items = afDB.list('users');
      this.getWorry('secretmyroom')
      this.getWorry('openmyroom')
  }

//비밀,오픈에 맞게 제목과 이름가져오기
  getWorry(room) {
    firebase.database().ref(room+ '/'+  this.authService.currentUserId).on('value', (snapshot) => {
      let result = snapshot.val();
      var i=0;
      this.userData = [];
      for(let k in result){
        this.userData.push({
          key : Object.keys(snapshot.val())[i],
          name : result[k].name,
          title : result[k].title,
          cate : result[k].cate
        });
        i++
      }
      this.userData.reverse();
    });
  }
// 삭제하기 팝업
  removeRoomAlert(roomkey,cate){
    let alert = this.alertCtrl.create({
      message: '삭제 하시겠습니까?',
      buttons: [
          {
              text: '네',
              handler: () => {
                this.removeRoom(roomkey,cate)
                console.log(roomkey,cate)
              }
          },
          {
              text: '아니오',
              handler: () => {
              }
          }]}).then(alert => {
              alert.present();
          });
  }
//삭제 실행부분
  removeRoom(roomkey,cate){
    this.afDB.object('openmyroom/' + this.user.uid + "/" + roomkey).remove();
    this.afDB.object('openroom/' + cate + "/" + roomkey).remove();
  }
// 읽기부분으로 넘어가기
  goToReadRoom(item){  
    this.router.navigate(['/worry-read', { readKey: item.key, cate: item.cate}]);
//    this.router.navigateByUrl('/worry-read');
  }

}